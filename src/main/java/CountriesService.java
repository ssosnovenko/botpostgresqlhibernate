import java.util.List;

public class CountriesService {
    private CountriesDao countriesDao = new CountriesDao();

    public CountriesService(){

    }

    public Countries findCountry(int id) {
        return countriesDao.findById(id);
    }

    public City findCity(int id) {
        return countriesDao.findCityById(id);
    }

    public Sight findSight(int id) {
        return countriesDao.findSightById(id);
    }

    public void saveCountry(Countries countries) {
        countriesDao.save(countries);
    }

    public void deleteCountry(Countries countries) {
        countriesDao.delete(countries);
    }

    public void deleteCountry(int id) {
        countriesDao.delete(id);
    }

    public void updateCountry(Countries countries) {
        countriesDao.update(countries);
    }

    public List<Countries> findAllRecords() {
        return countriesDao.findAll();
    }

    public int getNumOfLines() {
        return countriesDao.findAll().size();
    }
}

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "countries")
public class Countries {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int countries_id;

    @Column(name = "country")
    private String country;

    @Column(name = "capital")
    private String capital;

    @Column(name = "president")
    private String president;

    @Column(name = "population")
    private int population;

    @OneToMany(mappedBy = "countries", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<City> cities;

    public Countries() {
    }

    public Countries(String country, String capital, int population, String president) {
        this.country = country;
        this.capital = capital;
        this.population = population;
        this.president = president;
        cities = new ArrayList<City>();
    }

    public void addCity(City city){
        city.setCountries(this);
        cities.add(city);
    }

    public void removeCity(City city){
        cities.remove(city);
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities){
        this.cities = cities;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public String getCountry() {
        return country;
    }

    public String getCapital() {
        return capital;
    }

    public int getPopulation() {
        return population;
    }

    public String getPresident() {
        return president;
    }

    public int getId() {
        return countries_id;
    }

    @Override
    public String toString() {
        return "№"+ countries_id + "Страна "+ country + ", столица "+ capital + ", президент " + president + ", население " + population;
    }
}

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

public class Bot extends TelegramLongPollingBot {

    private String previousMessage;
    private String error = "";
    private CountriesService countriesService = new CountriesService();

    public void onUpdateReceived(Update update) {

        if(update.hasMessage()){
            if(update.getMessage().hasText()){
                switch (update.getMessage().getText()){
                    case "/start":
                        try {
                            execute(sendInlineKeyBoardMessage(update.getMessage().getChatId()));
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }
                }
                /*if(update.getMessage().getText().equals("/start")){
                    try {
                        execute(sendInlineKeyBoardMessage(update.getMessage().getChatId()));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }*/
            }
        }else if(update.hasCallbackQuery()){
            String callbackQueryData = update.getCallbackQuery().getData();

            try {
                if (callbackQueryData.equals("viewall")) {
                    List<Countries> countries = countriesService.findAllRecords();
                    execute(sendInlineKeyBoardMessageCountries(update.getCallbackQuery().getMessage().getChatId(), countries));
                }
                else if (callbackQueryData.startsWith("country")) {
                    Countries country = countriesService.findCountry(Integer.parseInt(callbackQueryData.replaceAll("country","")));
                    List<City> cities = country.getCities();
                    execute(new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setText("Вы выбрали "+country.getCountry()));
                    execute(sendInlineKeyBoardMessageCities(update.getCallbackQuery().getMessage().getChatId(), cities));
                }
                else if (callbackQueryData.startsWith("city")) {
                    City city = countriesService.findCity(Integer.parseInt(callbackQueryData.replaceAll("city","")));
                    List<Sight> sights = city.getSights();
                    execute(sendInlineKeyBoardMessageSights(update.getCallbackQuery().getMessage().getChatId(), sights));
                    /*Countries country = countriesService.findCountry(Integer.parseInt(callbackQueryData.replaceAll("country","")));
                    List<City> cities = country.getCities();*/
                }
                else if (callbackQueryData.startsWith("sight")) {
                    Sight sight = countriesService.findSight(Integer.parseInt(callbackQueryData.replaceAll("sight","")));
                    execute(new SendMessage().setChatId(update.getCallbackQuery().getMessage().getChatId()).setText(
                            "А тут пошел рассказ о достопримечательности "+sight.getName() +
                            ", которая находится в " + sight.getCity().getCity() +
                            ", имеет рейтинг на Гугл Картах - " + sight.getRating() +
                            " и " + sight.getFeedbnum() +
                            " отзывов, находится по координатам "+sight.getGeopos()+
                            ", а на посещение требуется Х часов."));
                }


            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    public static SendMessage sendInlineKeyBoardMessageSights(long chatId, List<Sight> sights) {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        for (Sight sight : sights) {
            List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();
            keyboardButtonsRow.add(new InlineKeyboardButton().setText(sight.getName()).setCallbackData("sight"+sight.getId()));
            rowList.add(keyboardButtonsRow);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Выберите интересующую вас достопримечательность").setReplyMarkup(inlineKeyboardMarkup);
    }

    public static SendMessage sendInlineKeyBoardMessageCities(long chatId, List<City> cities) {

        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<>();

        for (City city : cities) {
            List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();
            keyboardButtonsRow.add(new InlineKeyboardButton().setText(city.getCity()).setCallbackData("city"+city.getId()));
            rowList.add(keyboardButtonsRow);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Какой город вас интересует?").setReplyMarkup(inlineKeyboardMarkup);
    }

    public static SendMessage sendInlineKeyBoardMessageCountries(long chatId, List<Countries> countries) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowList = new ArrayList<List<InlineKeyboardButton>>();

        for (Countries country : countries) {
            List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<>();
            keyboardButtonsRow.add(new InlineKeyboardButton().setText(country.getCountry()).setCallbackData("country"+country.getId()));
            rowList.add(keyboardButtonsRow);
        }
        inlineKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Выберите страну, в которой вас интересуют достопримечательности").setReplyMarkup(inlineKeyboardMarkup);
    }

    public static SendMessage sendInlineKeyBoardMessage(long chatId) {
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<InlineKeyboardButton> keyboardButtonsRow = new ArrayList<InlineKeyboardButton>();
        keyboardButtonsRow.add(new InlineKeyboardButton().setText("Вывести список стран").setCallbackData("viewall"));

        List<List<InlineKeyboardButton>> rowList = new ArrayList<List<InlineKeyboardButton>>();
        rowList.add(keyboardButtonsRow);
        inlineKeyboardMarkup.setKeyboard(rowList);

        return new SendMessage().setChatId(chatId).setText("Приветствую! Я расскажу вам про достопримечательности разных стран и городов. "+
                "Чтобы вывести список стран нажмите на кнопку ниже").setReplyMarkup(inlineKeyboardMarkup);
    }

    public void onUpdatesReceived(List<Update> updates) {
        for (Update update : updates) onUpdateReceived(update);
    }

    public String getBotUsername() {
        return "Sosnovenko_bot";
    }

    public String getBotToken() {
        return "1090036410:AAFHjSrwkZ6vTs6i4k7-DKGtrEeTcTVWqE8";
    }
}

/*public static SendMessage sendInlineKeyBoardAdd(long chatId) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //replyKeyboardMarkup.setKeyboard(new ArrayList<KeyboardRow>(new ReplyKeyboard()))

        KeyboardRow keyboardRow1 = new KeyboardRow();
        KeyboardRow keyboardRow2 = new KeyboardRow();
        KeyboardRow keyboardRow3 = new KeyboardRow();
        keyboardRow1.add(new KeyboardButton().setText("Ввести страны"));
        keyboardRow1.add(new KeyboardButton().setText("Ввести столицу"));
        keyboardRow1.add(new KeyboardButton().setText("Ввести сто"));
        keyboardRow2.add(new KeyboardButton().setText("Ввести президента"));
        keyboardRow2.add(new KeyboardButton().setText("Ввести население"));
        keyboardRow3.add(new KeyboardButton().setText("Ввести население"));

        List<KeyboardRow> rowList = new ArrayList<KeyboardRow>();
        rowList.add(keyboardRow1);
        rowList.add(keyboardRow2);
        rowList.add(keyboardRow3);
        replyKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Добавим по порядку все записи:").setReplyMarkup(replyKeyboardMarkup);
    }*/

/*public static SendMessage sendInlineKeyBoardStart(long chatId) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        //replyKeyboardMarkup.setKeyboard(new ArrayList<KeyboardRow>(new ReplyKeyboard()))

        KeyboardRow keyboardRow1 = new KeyboardRow();
        KeyboardRow keyboardRow2 = new KeyboardRow();
        KeyboardRow keyboardRow3 = new KeyboardRow();
        KeyboardRow keyboardRow4 = new KeyboardRow();
        KeyboardRow keyboardRow5 = new KeyboardRow();
        KeyboardRow keyboardRow6 = new KeyboardRow();
        keyboardRow1.add(new KeyboardButton().setText("Показать все страны"));
        keyboardRow1.add(new KeyboardButton().setText("Найти страну по названию"));
        //keyboardRow1.add(new KeyboardButton().setText("Удалить страну"));
        //keyboardRow1.add(new KeyboardButton().setText("Добавить страну"));
        keyboardRow2.add(new KeyboardButton().setText("Показать города страны"));
        keyboardRow2.add(new KeyboardButton().setText("Найти город по названию"));
        //keyboardRow2.add(new KeyboardButton().setText("Удалить город"));
        //keyboardRow2.add(new KeyboardButton().setText("Добавить город"));
        keyboardRow3.add(new KeyboardButton().setText("Вывести список достопримечательностей города"));
        keyboardRow4.add(new KeyboardButton().setText("Удалить"));
        keyboardRow4.add(new KeyboardButton().setText("Добавить"));

        List<KeyboardRow> rowList = new ArrayList<KeyboardRow>();
        rowList.add(keyboardRow1);
        rowList.add(keyboardRow2);
        rowList.add(keyboardRow3);
        rowList.add(keyboardRow4);
        rowList.add(keyboardRow5);
        rowList.add(keyboardRow6);
        replyKeyboardMarkup.setKeyboard(rowList);
        return new SendMessage().setChatId(chatId).setText("Что требуется сделать?").setReplyMarkup(replyKeyboardMarkup);
    }*/

/*    /*public void onUpdateReceived(Update update) {
        //System.out.println("we have an update!");

        try {
            if (update.hasMessage() && update.getMessage().hasText()) {
                Message inMessage = update.getMessage();
                SendMessage outMessage = new SendMessage();
                outMessage.setChatId(inMessage.getChatId());
                String messageText = inMessage.getText();

                if (!error.equals("")) {
                    outMessage.setText(error);
                    execute(outMessage);
                }
                else if (messageText.equals("/start")) {
                    outMessage.setText(
                            "Это простая база данных стран\n" +
                            "с использованием " + BotDB.DriverName + " driver.\n\n" +
                            "Команды:\n" +
                            "/add\n" +
                            "/del\n" +
                            "/view\n");
                    execute(outMessage);
                }
                else if (messageText.equals("/add")) {
                    outMessage.setText("Введите новую запись в следующем формате:\n " +
                            "Страна Столица Население(млн. чел. - число)  ");
                    execute(outMessage);
                }
                else if (messageText.equals("/del")) {
                    outMessage.setText("Введите индекс удаляемой записи");
                    execute(outMessage);
                }
                else if (messageText.equals("/view")) {
                    List<String> dbLines = BotDB.ReadLine();
                    if (dbLines.size()>0)
                    for (String line : dbLines) {
                        outMessage.setText(line);
                        execute(outMessage);
                    }
                    else {
                        outMessage.setText("В БД нет ни одной записи");
                        execute(outMessage);
                    }
                }
                else if (previousMessage.equals("/add")) {
                    String[] lines =  messageText.split(" ");
                    if (lines.length ==3) {
                        try {
                            BotDB.WriteLine(lines[0], lines[1], Integer.parseInt(lines[2]), inMessage.getFrom().getUserName());

                            outMessage.setText("Запись добавлена");
                            execute(outMessage);
                        }catch (NumberFormatException e)
                        {
                            outMessage.setText("Некорректно введены данные населения");
                            execute(outMessage);
                        }

                    }
                    else {
                        outMessage.setText("Кол-во слов в строке превышает необходимое");
                        execute(outMessage);
                    }
                }
                else if (previousMessage.equals("/del")) {
                    try {
                        int id =  Integer.parseInt(messageText);
                        BotDB.DeleteLine(id);
                        outMessage.setText("Если запись с таким номером существовала, она была удалена");
                        execute(outMessage);
                    } catch (NumberFormatException e)
                    {
                        outMessage.setText("Введено некорректное число");
                        execute(outMessage);
                    }

                }
                else {
                    outMessage.setText("Убедитесь, что перед вводом данных была введена команда");
                    execute(outMessage);
                }

                previousMessage = messageText;
                error = "";
                /*String userName = inMessage.getForwardFrom().getUserName();
                int id = inMessage.getForwardFrom().getId();


            }
        } catch (TelegramApiException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            error = e.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            error = "Ошибка введенных данных";
        }
    }*/
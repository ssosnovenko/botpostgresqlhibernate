import javax.persistence.*;

@Entity
@Table(name = "sights")

public class Sight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int sight_id;

    @Column (name = "name")
    private String name;

    @Column (name = "rating")
    private double rating;

    @Column (name = "feedbnum")
    private int feedbnum;

    @Column (name = "geopos")
    private String geopos;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "city_id", referencedColumnName="city_id")
    private City city;

    public Sight(){

    }

    public Sight(String name, double rating, int feedbnum, String geopos){
        this.name = name;
        this.rating = rating;
        this.feedbnum = feedbnum;
        this.geopos = geopos;

    }

    public City getCity() {
        return city;
    }

    public double getRating() {
        return rating;
    }

    public int getFeedbnum() {
        return feedbnum;
    }

    public int getId() {
        return sight_id;
    }

    public String getGeopos() {
        return geopos;
    }

    public String getName() {
        return name;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public void setFeedbnum(int feedbnum) {
        this.feedbnum = feedbnum;
    }

    public void setGeopos(String geopos) {
        this.geopos = geopos;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Sight{" +
                "name='" + name + '\'' +
                '}';
    }
}

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "cities")

public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int city_id;

    @Column (name = "city")
    private String city;

    @Column (name = "mayor")
    private String mayor;

    @Column (name = "population")
    private int population;

    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Sight> sights;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "countries_id", referencedColumnName="countries_id")
    private Countries countries;

    public City(){

    }

    public City(String city, String mayor, int population){
        this.city = city;
        this.mayor = mayor;
        this.population = population;
        sights = new ArrayList<Sight>();
    }

    public String getCity() {
        return city;
    }

    public String getMayor() {
        return mayor;
    }

    public int getId() {
        return city_id;
    }

    public int getPopulation() {
        return population;
    }

    public Countries getCountries() {
        return countries;
    }

    public List<Sight> getSights() {
        return sights;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setMayor(String mayor) {
        this.mayor = mayor;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public void setCountries(Countries countries) {
        this.countries = countries;
    }

    public void addSight(Sight sight){
        sight.setCity(this);
        sights.add(sight);
    }

    @Override
    public String toString() {
        return "City{" +
                "city='" + city + '\'' +
                '}';
    }
}

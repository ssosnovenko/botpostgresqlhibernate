import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;

public class CountriesDao {
    public Countries findById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Countries.class, id);
    }

    public void save(Countries countries) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(countries);
        transaction.commit();
        session.close();
    }

    public void update(Countries countries) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(countries);
        transaction.commit();
        session.close();
    }

    public void delete(Countries countries) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(countries);
        transaction.commit();
        session.close();
    }

    public void delete(int id) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Countries countries = session.get(Countries.class, id);
        Transaction transaction = session.beginTransaction();
        session.delete(countries);
        transaction.commit();
        session.close();
    }


    public City findCityById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(City.class, id);
    }

    public Sight findSightById(int id) {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Sight.class, id);
    }

    public List<Countries> findAll() {
        List<Countries> countries = (List<Countries>)  HibernateSessionFactoryUtil.getSessionFactory().openSession().createQuery("From Countries").list();
        return countries;
    }
}
